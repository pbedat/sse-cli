/**
 * Parses a list of headers to a headers object
 * @param headers headers in the form "<HEADER>: <VALUE>"
 * @example parseHeaders(["X-Foo: Bar"]) == { errors: undefined, headers: { "X-Foo": "Bar" } }
 * @returns .errors when some headers could not be parsed and successfully parsed headers in .headersObject
 */
export function parseHeaders(headers: string[]) {
  // pattern for strings like "Accept-Language: de-DE"
  const PATTERN = /([^:]+):(.+)/;

  // headers that cannot be parsed will be gathered here
  const malformedHeaders: string[] = [];

  const headersObject = headers.reduce((obj, header) => {
    const match = PATTERN.exec(header);
    if (!match) {
      malformedHeaders.push(header);
      return obj;
    }

    const [_, headerName, headerValue] = match;

    return { ...obj, [headerName]: headerValue };
  }, {});

  return {
    errors: malformedHeaders.length ? malformedHeaders : undefined,
    headers: headersObject
  };
}
