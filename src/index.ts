import yargs from "yargs";

const { argv } = yargs
  .option("url", {
    type: "string",
    describe: "the url of the event source",
    required: true
  } as yargs.Options)
  .option("withCredentials", {
    type: "boolean",
    describe: "sets the with-credentials flag of the event source"
  })
  .option("headers", {
    type: "array",
    default: [],
    describe:
      "set headers when connecting to the event source: --headers 'Accept-Language: de-DE'",
    alias: "h"
  } as yargs.Options)
  .option("sslVerify", {
    type: "boolean",
    default: true,
    describe: "rejects unauthorized https event sources"
  } as yargs.Options)
  .option("proxy", {
    type: "string",
    describe: "a proxy server to be used for the event source connection"
  });

import { parseHeaders } from "./headers";

const parsedHeaders = parseHeaders(argv.headers as string[]);

if (parsedHeaders.errors) {
  process.stderr.write(
    `error: cannot parse headers "${parsedHeaders.errors.join(", ")}"`
  );
  process.exit(1);
}

import EventSource from "eventsource";

const events = new EventSource(argv.url as string, {
  withCredentials: argv.withCredentials as boolean,
  headers: parsedHeaders.headers,
  proxy: argv.proxy as string,
  rejectUnauthorized: argv.sslVerify as boolean
});

events.onopen = () => {
  if (process.stdout.isTTY) {
    switch(events.readyState) {
      case events.CONNECTING:
        process.stdout.write(`event source connecting\n`);
        break;
      case events.OPEN:
        process.stdout.write(`event source connected\n`);
        break;
      default:
        process.stdout.write(`event source (${events.readyState})}\n`);
    }
  }
};

events.onerror = (err: any) => {
  if (!err.message) {
    process.stderr.write(`error: connection failed\n`);
    return;
  }
  process.stderr.write(`error: ${err.message} (${err.status})\n`);
};

events.onmessage = ev => {
  process.stdout.write(`${ev.data}\n`);
};
