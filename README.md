# sse-cli

Consume server-sent events on a command line

## Usage

```
$ npx sse-cli --help
npx: installed 66 in 7.075s
Options:
  --help             Show help                                         [boolean]
  --version          Show version number                               [boolean]
  --url              the url of the event source             [string] [required]
  --withCredentials  sets the with-credentials flag of the event source[boolean]
  --headers, -h      set headers when connecting to the event source: --headers
                     'Accept-Language: de-DE'              [array] [default: []]
  --sslVerify        rejects unauthorized https event sources
                                                       [boolean] [default: true]
  --proxy            a proxy server to be used for the event source connection
                                                                        [string]
```